
import fs from 'fs'
import path from 'path'

import PGAura from 'zunzun/pg-aura/index.mjs'

import FlyCfg from 'zunzun/flycfg/index.mjs'
import LibFlyAuth from 'zunzun/authing/index.mjs'
import CLIUtil from 'zunzun/flyutil/cli.mjs'

import bcrypt from 'bcrypt'

export default class FlyCMS {
  static async run(cfg) {
    try {
      if (process.argv.length > 3) {
        switch (process.argv[3]) {
          case "add-admin":
            let zuncfg = await FlyCfg.project(process.cwd());
            let flycms_config = undefined;
            console.log(zuncfg);
            if (Array.isArray(zuncfg.app_cfg.services)) {
              for (let srv of zuncfg.app_cfg.services) {
                if (srv.name == "flycms") {
                  flycms_config = srv.config
                }
              }
            }

            console.log(flycms_config);

            const pg_cfg = JSON.parse(fs.readFileSync(path.resolve(
              process.cwd(), '.pg-env.json'
            )));
            const pg = await PGAura.connect({
              database: pg_cfg.db_name,
              user: pg_cfg.db_user,
              pass: pg_cfg.db_pwd
            });

            const flyadmin = await LibFlyAuth.construct(pg, null, flycms_config);

            let admin_username = undefined;
            let admin_password = undefined;
            if (process.argv.length > 4 && process.argv[4] == "--testing") {
              admin_username = "admin";
              admin_password = "123456";
            } else {
              let ir = CLIUtil.InputReader();
              console.log("");
              console.log("\x1b[36m>>> Set username and password for you new administrator user: <<<\x1b[0m");

              admin_username = await ir.questionSync('Username[admin]: ');
              if (!admin_username) admin_username = "admin";

              admin_password = await ir.questionSync('Password: ', true);
              while (admin_password.length < 6) {
                console.log("");
                console.log(">!> Your password must be at least 6 characters long! <!<");
                admin_password = await ir.questionSync('Password: ', true);
              }

              ir.close();
            }

            const existing = await flyadmin.accounts_table.select("*", "username = $1", [admin_username]);
            if (existing.length == 0) {
              const salt = bcrypt.genSaltSync(10);
              await flyadmin.accounts_table.insert([
                `username`, `password`, `privileges`
              ], [`$1`, `$2`, `$3`], [
                admin_username,
                bcrypt.hashSync(admin_password, salt),
                ["super"]
              ]);
              console.log(`Administrator user "${admin_username}" successfuly added!`);
            } else {
              console.log(`Username "${admin_username}" already taken!`);
              process.exit();
            }

            await pg.disconnect()
            break;
          default:
            console.log(`Invalid command ${process.argv[3]}`)
            process.exit();
        }

      } else {
        console.log("No command");
        process.exit();
      }



    } catch (e) {
      console.error(e.stack);
    }
  }
}
